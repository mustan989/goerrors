package goerrors

import (
	"fmt"
	"net/http"
)

// HTTPCodeWithBody returns http status code & struct to be marshalled to json based on error type
func HTTPCodeWithBody(err error) (code int, body interface{}) {
	return HTTPCode(err), HTTPBody(err)
}

// HTTPCode returns http status code based on error type
func HTTPCode(err error) int {
	switch e := err.(type) {
	case *errBadReq:
		return httpCodes[e.t]
	case *errNotValid:
		return httpCodes[badRequest]
	case *errInternal:
		return httpCodes[e.t]
	case *base:
		return httpCodes[e.t]
	default:
		return httpCodes[unknown]
	}
}

// HTTPBody returns Body struct to be marshalled to json based on error type
func HTTPBody(err error) (body interface{}) {
	switch e := err.(type) {
	case *errBadReq:
		return badRequestBody(e)
	case *errInternal:
		return internalBody(e)
	case *errNotValid:
		return notValidBody(e)
	case *base:
		return baseBody(e)
	case *httpBase:
		return baseHTTPBody(e)
	default:
		return internalBody(&errInternal{Info: "unknown error"})
	}
}

// Default errors
var (
	// ErrUnauthorized unauthorized access. Better to provide more info using Unauthorized
	ErrUnauthorized = Unauthorized("")

	// ErrForbidden not enough rights. Better to provide more info using Forbidden(msg string)
	ErrForbidden = Forbidden("")

	// ErrMethodNotAllowed method not allowed. Better to provide more info using MethodNotAllowed(msg string)
	ErrMethodNotAllowed = MethodNotAllowed("")

	// ErrUnsupportedMediaType media type is not supported. Better to provide supported types using UnsupportedMediaType(msg string). E.g.:
	//	UnsupportedMediaType(fmt.Sprintf("%s is not supported, try to send 'application/json'", req.Header().Get("Content-Type")))
	ErrUnsupportedMediaType = UnsupportedMediaType("")

	// ErrImATeapot I'm a Teapot ;)
	ErrImATeapot = ImATeapot()

	// TODO: add ErrTooManyRequests

	// ErrNotImplemented method/function is not implemented. Better to provide more info using NotImplemented(msg string)
	ErrNotImplemented = NotImplemented("")

	// TODO: add ErrBadGateway

	// ErrServiceUnavailable service is not available. Strongly recommended to use ServiceUnavailable & provide location or service name which is not available
	ErrServiceUnavailable = ServiceUnavailable("")

	// TODO: add ErrGatewayTimeout
)

// Unauthorized returns 'unauthorized' error with msg
func Unauthorized(msg string) error {
	return &httpBase{base{t: httpUnauthorized, msg: msg}}
}

// Unauthorizedf returns 'unauthorized' error with formatted msg
func Unauthorizedf(format string, args ...interface{}) error {
	return &httpBase{base{t: httpUnauthorized, msg: fmt.Sprintf(format, args...)}}
}

// Forbidden returns 'forbidden' error with msg
func Forbidden(msg string) error {
	return &httpBase{base{t: httpForbidden, msg: msg}}
}

// Forbiddenf returns 'forbidden' error with formatted msg
func Forbiddenf(format string, args ...interface{}) error {
	return &httpBase{base{t: httpForbidden, msg: fmt.Sprintf(format, args...)}}
}

// MethodNotAllowed returns 'method not allowed' error with msg
func MethodNotAllowed(msg string) error {
	return &httpBase{base{t: httpMethodNotAllowed, msg: msg}}
}

// MethodNotAllowedf returns 'method not allowed' error with formatted msg
func MethodNotAllowedf(format string, args ...interface{}) error {
	return &httpBase{base{t: httpMethodNotAllowed, msg: fmt.Sprintf(format, args...)}}
}

// UnsupportedMediaType returns 'unsupported media type' error with msg
func UnsupportedMediaType(msg string) error {
	return &httpBase{base{t: httpUnsupportedMediaType, msg: msg}}
}

// UnsupportedMediaTypef returns 'unsupported media type' error with formatted msg
func UnsupportedMediaTypef(format string, args ...interface{}) error {
	return &httpBase{base{t: httpUnsupportedMediaType, msg: fmt.Sprintf(format, args...)}}
}

// ImATeapot returns 'I'm a teapot' error
func ImATeapot() error {
	return &httpBase{base{t: httpImATeapot}}
}

// TooManyRequests returns 'too many requests' error with msg
func TooManyRequests(msg string) error {
	return &httpBase{base{t: httpTooManyRequests, msg: msg}}
}

// TooManyRequestsf returns 'too many requests' error with formatted msg
func TooManyRequestsf(format string, args ...interface{}) error {
	return &httpBase{base{t: httpTooManyRequests, msg: fmt.Sprintf(format, args...)}}
}

// NotImplemented returns 'not implemented' error with msg
func NotImplemented(msg string) error {
	return &httpBase{base{t: httpNotImplemented, msg: msg}}
}

// NotImplementedf returns 'not implemented' error with formatted msg
func NotImplementedf(format string, args ...interface{}) error {
	return &httpBase{base{t: httpNotImplemented, msg: fmt.Sprintf(format, args...)}}
}

// BadGateway returns 'bad gateway' error with msg
func BadGateway(msg string) error {
	return &httpBase{base{t: httpBadGateway, msg: msg}}
}

// BadGatewayf returns 'bad gateway' error with formatted msg
func BadGatewayf(format string, args ...interface{}) error {
	return &httpBase{base{t: httpBadGateway, msg: fmt.Sprintf(format, args...)}}
}

// ServiceUnavailable returns 'service unavailable' error with msg
func ServiceUnavailable(msg string) error {
	return &httpBase{base{t: httpServiceUnavailable, msg: msg}}
}

// ServiceUnavailablef returns 'service unavailable' error with formatted msg
func ServiceUnavailablef(format string, args ...interface{}) error {
	return &httpBase{base{t: httpServiceUnavailable, msg: fmt.Sprintf(format, args...)}}
}

// GatewayTimeout returns 'gateway timeout' error with msg
func GatewayTimeout(msg string) error {
	return &httpBase{base{t: httpGatewayTimeout, msg: msg}}
}

// GatewayTimeoutf returns 'gateway timeout' error with formatted msg
func GatewayTimeoutf(format string, args ...interface{}) error {
	return &httpBase{base{t: httpGatewayTimeout, msg: fmt.Sprintf(format, args...)}}
}

func badRequestBody(err *errBadReq) interface{} {
	return Body{
		Msg:    err.base.msg,
		Errors: err.Invalid,
	}
}

func internalBody(err *errInternal) interface{} {
	return Body{
		Msg:    err.Info,
		Errors: err.Paths,
	}
}

func notValidBody(err *errNotValid) interface{} {
	return Body{
		Msg:    err.base.msg,
		Errors: err.Invalid,
	}
}

func baseHTTPBody(err *httpBase) interface{} {
	return Body{
		Msg: err.base.msg,
	}
}

func baseBody(err *base) interface{} {
	return Body{
		Msg: err.msg,
	}
}

// Body default structure for error's json representation
type Body struct {
	Msg    string   `json:"msg,omitempty"`
	Errors []string `json:"errors,omitempty"`
}

type httpBase struct {
	base
}

func (b *httpBase) Error() string {
	n, ok := tNames[b.t]
	if !ok {
		n = httpName(b)
	}
	if len(b.msg) == 0 {
		return n
	}
	return fmt.Sprintf("%s: %s", httpName(b), b.msg)
}

// HTTP error types
const (
	httpUnauthorized t = 100 + iota
	// TODO: implement
	httpPaymentRequired
	httpForbidden
	httpMethodNotAllowed
	// TODO: implement
	httpNotAcceptable
	// TODO: implement
	httpProxyAuthorizationRequired
	// TODO: implement
	httpGone
	// TODO: implement
	httpLengthRequired
	// TODO: implement
	httpPreconditionFailed
	// TODO: implement
	httpPayloadTooLarge
	// TODO: implement
	httpURITooLong
	httpUnsupportedMediaType
	// TODO: implement
	httpRequestedRangeNotSatisfiable
	// TODO: implement
	httpExpectationFailed
	httpImATeapot
	// TODO: implement
	httpMisdirectedRequest
	// TODO: implement
	httpUnprocessableEntity
	// TODO: implement
	httpLocked
	// TODO: implement
	httpFailedDependency
	// TODO: implement
	httpUpgradeRequired
	// TODO: implement
	httpPreconditionRequired
	httpTooManyRequests
	// TODO: implement
	httpRequestHeaderFieldsTooLarge
	// TODO: implement
	httpConnectionClosedWithoutResponse
	// TODO: implement
	httpUnavailableForLegalReasons
	// TODO: implement
	httpClientClosedRequest

	httpNotImplemented
	httpBadGateway
	httpServiceUnavailable
	httpGatewayTimeout
	// TODO: implement
	httpHTTPVersionNotSupported
	// TODO: implement
	httpVariantAlsoNegotiates
	// TODO: implement
	httpInsufficientStorage
	// TODO: implement
	httpLoopDetected
	// TODO: implement
	httpNotExtended
	// TODO: implement
	httpNetworkAuthenticationRequired
	// TODO: implement
	httpNetworkConnectTimeoutError
)

// het http error type
func het(err error) t {
	switch e := err.(type) {
	case *httpBase:
		return e.base.t
	default:
		return et(e)
	}
}

// httpName returns name of http error
func httpName(err error) string {
	return httpTNames[het(err)]
}

func hTName(t t) string {
	if t < 100 {
		return tName(t)
	}
	return httpTNames[t]
}

var (
	httpCodes = map[t]int{
		badRequest:               http.StatusBadRequest,
		httpUnauthorized:         http.StatusUnauthorized,
		httpForbidden:            http.StatusForbidden,
		notFound:                 http.StatusNotFound,
		httpMethodNotAllowed:     http.StatusMethodNotAllowed,
		requestTimeout:           http.StatusRequestTimeout,
		conflict:                 http.StatusConflict,
		httpUnsupportedMediaType: http.StatusUnsupportedMediaType,
		httpImATeapot:            http.StatusTeapot,
		httpTooManyRequests:      http.StatusTooManyRequests,

		internal:               http.StatusInternalServerError,
		httpNotImplemented:     http.StatusNotImplemented,
		httpBadGateway:         http.StatusBadGateway,
		httpServiceUnavailable: http.StatusServiceUnavailable,
		httpGatewayTimeout:     http.StatusGatewayTimeout,

		unknown: http.StatusInternalServerError,
	}

	httpTNames = map[t]string{
		httpUnauthorized:         "unauthorized",
		httpForbidden:            "forbidden",
		httpMethodNotAllowed:     "method not allowed",
		httpUnsupportedMediaType: "unsupported media type",
		httpImATeapot:            "I'm teapot :)",
		httpTooManyRequests:      "too many requests",

		httpNotImplemented:     "not implemented",
		httpBadGateway:         "bad gateway",
		httpServiceUnavailable: "service unavailable",
		httpGatewayTimeout:     "gateway timeout",
	}
)
