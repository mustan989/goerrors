package goerrors

import (
	"fmt"
	"strings"
)

// Default errors
var (
	// ErrUnknown unknown behavior error
	ErrUnknown = Unknown()

	// ErrBadRequest bad request made by requester. Better to provide more info using BadRequest(details ...string)
	ErrBadRequest = BadRequest()

	// ErrNotFound requested resource/data not found
	ErrNotFound = NotFound("")

	// ErrRequestTimeout request timed out
	ErrRequestTimeout = RequestTimeout("")

	// ErrConflict data conflict occurred
	ErrConflict = Conflict("")

	// ErrInternal internal error. Better to provide more info using Internal(description string, details ...string)
	ErrInternal = Internal("")

	ErrNotValid = NotValid("")
)

// Unknown represents unknown behavior
func Unknown() error {
	return &base{t: unknown}
}

// BadRequest returns 'request' error with invalid cases. E.g.:
//  e2 := BadRequest()
//  fmt.Println(e2)		// bad request
//
//  e1 := BadRequest("id must be numeric value", "name must not be less than 2 symbols")
//	fmt.Println(e1)		// bad request: id must be numeric value; name must not be less than 2 symbols
func BadRequest(invalid ...string) error {
	return &errBadReq{
		base:    base{t: badRequest},
		Invalid: invalid,
	}
}

// NotFound returns 'not found' error with msg
func NotFound(msg string) error {
	return &base{t: notFound, msg: msg}
}

// NotFoundf returns 'not found' error with formatted msg
func NotFoundf(format string, args ...interface{}) error {
	return &base{t: notFound, msg: fmt.Sprintf(format, args...)}
}

// RequestTimeout returns 'request timeout' error with msg
func RequestTimeout(msg string) error {
	return &base{t: requestTimeout, msg: msg}
}

// RequestTimeoutf returns 'request timeout' error with formatted msg
func RequestTimeoutf(format string, args ...interface{}) error {
	return &base{t: requestTimeout, msg: fmt.Sprintf(format, args...)}
}

// Conflict returns 'conflict' error with msg
func Conflict(msg string) error {
	return &base{t: conflict, msg: msg}
}

// Conflictf returns 'conflict' error with formatted msg
func Conflictf(format string, args ...interface{}) error {
	return &base{t: conflict, msg: fmt.Sprintf(format, args...)}
}

// Internal returns 'internal' error with msg & error occurred location. E.g.:
//	e1 := Internal("user service error")
//	fmt.Println(e1)		// internal: user service error
//
//	e2 := Internal("user service error", "user.service", "user.repo", "repo.GetByID")
//	fmt.Println(e2)		// internal: user service error at user.service at user.repo at repo.GetByID
func Internal(info string, paths ...string) error {
	return &errInternal{
		base:  base{t: internal},
		Info:  info,
		Paths: paths,
	}
}

// Internalf returns 'internal' error with formatted msg
func Internalf(format string, args ...interface{}) error {
	return &errInternal{
		base: base{t: internal},
		Info: fmt.Sprintf(format, args...),
	}
}

func NotValid(invalid ...string) error {
	return &errNotValid{
		errBadReq: &errBadReq{
			base:    base{t: notValid},
			Invalid: invalid,
		},
	}
}

func NotValidf(msg string, args ...interface{}) error {
	return &errNotValid{
		errBadReq: &errBadReq{
			base:    base{t: notValid},
			Invalid: []string{fmt.Sprintf(msg, args...)},
		},
	}
}

type base struct {
	t   t
	msg string
}

func (b *base) Error() string {
	if len(b.msg) == 0 {
		return name(b)
	}
	return fmt.Sprintf("%s: %s", name(b), b.msg)
}

type errBadReq struct {
	base
	Invalid []string
}

func (e *errBadReq) Error() string {
	if len(e.Invalid) == 0 {
		return name(e)
	}
	return fmt.Sprintf("%s: %s", name(e), strings.Join(e.Invalid, "; "))
}

type errInternal struct {
	base
	Info  string
	Paths []string
}

func (e *errInternal) Error() string {
	if len(e.Info) == 0 {
		return name(e)
	}
	if len(e.Paths) == 0 {
		return fmt.Sprintf("%s: %s", name(e), e.Info)
	}
	return fmt.Sprintf("%s: %s at %s", name(e), e.Info, strings.Join(e.Paths, " at "))
}

type errNotValid struct {
	*errBadReq
}

func (e *errNotValid) Error() string {
	if len(e.Invalid) == 0 {
		return name(e)
	}
	return fmt.Sprintf("%s: %s", name(e), strings.Join(e.Invalid, "; "))
}

type t int

// Error types
const (
	unknown t = iota
	badRequest
	notFound
	requestTimeout
	conflict
	internal
	notValid
)

// et error type
func et(err error) t {
	switch e := err.(type) {
	case *errBadReq:
		return e.base.t
	case *errInternal:
		return e.base.t
	case *base:
		return e.t
	default:
		return unknown
	}
}

// name returns name of error
func name(err error) string {
	return tName(et(err))
}

func tName(t t) string {
	return tNames[t]
}

var tNames = map[t]string{
	unknown:        "unknown",
	badRequest:     "bad request",
	notFound:       "not found",
	requestTimeout: "request timeout",
	conflict:       "conflict",
	internal:       "internal",
	notValid:       "not valid",
}
