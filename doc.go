/*
Package goerrors
contains custom errors for communication between different levels of application

Main points:

- exported vars with prefix 'Err' provide errors with default values. E.g.: ErrUnknown()

- in most cases it is better to use functions without 'Err' prefix to provide more information about error. E.g.: Internal(

- useful functions are HTTPCode to represent error as http status code,
HTTPBody to represent error as structure to be marshalled to json &
HTTPCodeWithBody which joins both functions

Example below shows http Rest API service logic

HTTP Handler (decoding, validating request):
	func (h *UserHandler) GetByID(c echo.Context) error {
		// get path parameter /api/v1/users/{id}
		id := c.Param("id")

		// convert to int
		atoi, err := strconv.Atoi(id)
		if err != nil {
			// return custom error's http response representation
			return c.JSON(goerrors.HTTPCodeWithBody(
				goerrors.BadRequest("id must be numeric value"),
			))
		}

		req := GetUserRequest{ID: atoi}

		// do service request
		res, err := h.svc.GetByID(c.Request().Context(), req)
		if err != nil {
			// represent error as http response
			return c.JSON(goerrors.HTTPCodeWithBody(err))
		}

		return c.JSON(http.StatusOK, res)
	}

Service (business logic):
	func (s *UserSvc) GetByID(ctx context.Context, req GetUserRequest) (GetUserResponse, error) {
		// ...

		// get data from repository
		user, err := s.repo.GetByID(ctx, req.ID)
		if err != nil {
			// log & return error
			s.log.Printf("user with id %d not found", req.ID)
			return nil, err
		}

		// ...
	}

Repository (provide database interaction):
	func (r *UserRepo) GetByID(ctx context.Context, id int) (*User, error) {
		// get data
		row, err := r.db.QueryContext(ctx, "select name, email from users u where u.id=$1", id)
		if err != nil {
			// check if query returned no data
			if errors.Is(err, sql.ErrNoRows) {
				// return custom error to handle it on top level
				return nil, goerrors.NotFound("user with entered id not found")
			}
			return nil, err
		}

		// ...
	}

Finally, error will be returned to client as http response
	404 - Not Found
	{
		"msg": "user with entered id not found"
	}
*/
package goerrors
