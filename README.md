# go errors

## Description

goerrors package provides custom errors for communication between different levels of application


[//]: # (TODO: add badges)
<!--
## Badges

On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the
project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.
-->

## Installation

```shell
go get -u gitlab.com/mustan989/goerrors
```

[//]: # (TODO: add usages)
<!--
## Usage

Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of
usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably
include in the README.
-->

## Support

Any questions can be asked in repository issues or directly to my email mustan989@outlook.com


[//]: # (TODO: add roadmap)
<!--
## Roadmap

If you have ideas for releases in the future, it is a good idea to list them in the README.
-->

## Contributing

Project is open for contributing, you can do it by simple steps:

0. Fork repository
1. Create branch for Your Feature `git checkout -b feat/amazingFeature`
2. Commit `git commit -m "feat: added amazing feature`
3. Push `git push origin feat/amazingFeature`
4. Create Pull Request

## Authors and acknowledgment

Current Contributors:

* @mustan989 - mustan989@outlook.com

[//]: # (TODO: add licence & status)
<!--## License

For open source projects, say how it is licensed.
## Project status

If you have run out of energy or time for your project, put a note at the top of the README saying that development has
slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or
owner, allowing your project to keep going. You can also make an explicit request for maintainers.
-->

